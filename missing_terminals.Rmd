---
title: "Missing Terminals"
output:
  MetroTransitr::metro_report: default
  MetroTransitr::metro_html: default
---

```{r setup, include=FALSE, cache=TRUE}
library(data.table)
library(ggplot2)
library(RODBC)
dat_0 <- readRDS("Data/dat.RDS")
dat_0[, schedule := as.ITime(SCHEDULED_TIME)]
setkey(dat_0, CALENDAR_ID, BLOCK_ABBR, BLOCK_STOP_ORDER)
```

## Missing Terminal vs Stop Activity
Isolating terminals on the A-line, we have here an example of a block with one missing terminal. 
 
```{r, echo = FALSE, cache=TRUE, message=FALSE}
set.seed(3580)
dat <- dat_0[ROUTE_ABBR == 921][CALENDAR_ID %in% sample(unique(CALENDAR_ID), 1)][BLOCK_ABBR %in% sample(unique(BLOCK_ABBR), 1)]
test <- dat[TRIP_ID != shift(TRIP_ID, type = "lag"), 
            .(CALENDAR_ID, 
              BLOCK_ABBR, 
              ROUTE_ABBR, 
              TRIP_ID, 
              BLOCK_STOP_ORDER, 
              TIME_POINT_ABBR, 
              TIME_PT_NAME, 
              SCHEDULED_TIME, 
              ACTUAL_ARRIVAL_TIME, 
              geoNode_lat, 
              geoNode_lon, 
              ACTUAL_DEPARTURE_TIME, 
              a_source = "ADHERENCE", 
              d_source = "ADHERENCE")]
test[, .(TRIP_ID, 
         BLOCK_STOP = BLOCK_STOP_ORDER, 
         TIME_POINT = TIME_POINT_ABBR, 
         ARRIVAL = ACTUAL_ARRIVAL_TIME, 
         DEPART = ACTUAL_DEPARTURE_TIME, 
         geoNode_lat, geoNode_lon)]
paste0("Date = ", test[, unique(CALENDAR_ID)])
paste0("Block = ", test[, unique(BLOCK_ABBR)])
```

Taking a closer look at the adherance records between block stop 182 and 222, there are many time points missing records. 

```{r, echo = FALSE, cache=TRUE, message=FALSE}
dat[between(BLOCK_STOP_ORDER, 182, 222), 
    .(TRIP_ID, BLOCK_STOP_ORDER, TIME_POINT_ABBR, ARRIVAL = ACTUAL_ARRIVAL_TIME, DEPART = ACTUAL_DEPARTURE_TIME)]
```

The missing records are bounded in time between 37766 and 39237. Looking at the stop-activity records between those time points we find a handfull of records, however, no courtesy stops we can assign to the missing terminal.  

```{r, echo = FALSE, cache=TRUE, message=FALSE}
date <- test[is.na(ACTUAL_ARRIVAL_TIME), paste0(1, format(unique(CALENDAR_ID), "%Y%m%d"))]
block <- test[is.na(ACTUAL_ARRIVAL_TIME), unique(BLOCK_ABBR)]

ch <- odbcConnect("ETL_test", uid = getOption("odbc.uid"), pwd = getOption("odbc.pwd"))
query <- sprintf("select * from MT_LoggedMessages with (nolock) where 
                  CALENDAR_ID = %s and 
                  LM_BLOCK_ABBR = %s and
                  MESSAGE_TYPE_ID = 116", date, block)
dat_etl <- as.data.table(sqlQuery(ch, query))
close(ch)
dat_etl[, ':='(LATITUDE = LATITUDE/1e7, LONGITUDE = LONGITUDE/1e7)]

stop_activity <- dat_etl[between(MDT_TIMESTAMP, 37766, 39237, incbounds = T), .(MDT_TIMESTAMP, STOP_OFFSET, CAT_1, CAT_2, CAT_3, LATITUDE, LONGITUDE)]
stop_activity
```

There 













